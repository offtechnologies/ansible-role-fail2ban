import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('file', [
  ("/etc/fail2ban/filter.d/traefik-auth.conf"),
  ("/etc/fail2ban/jail.d/traefik.conf"),
  ("/etc/fail2ban/fail2ban.conf"),
  ("/etc/fail2ban/jail.conf")
])
def test_files(host, file):
    file = host.file(file)

    assert file.exists
    assert file.user == 'root'
    assert file.group == 'root'


@pytest.mark.parametrize('pkg', [
  'fail2ban'
])
def test_pkg(host, pkg):
    package = host.package(pkg)
    assert package.is_installed


def test_fail2ban_is_enabled(host):
    fail2ban = host.service("fail2ban")
    assert fail2ban.is_enabled
