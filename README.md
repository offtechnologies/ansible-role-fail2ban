ansible-role-fail2ban
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-fail2ban/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-fail2ban/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies
[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Manages fail2ban on Debian systems.

Requirements
------------
None

Role Variables
--------------

see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------

None

Example Playbook
----------------

```yaml
---

- name: Configure dev system(s)
  hosts: all
  roles:
    - { role: ansible-role-fail2ban }
  vars:
    fail2ban_services::
      - name: "traefik-auth"
        enabled: "true"
        port: "http,https"
        filter: "traefik-auth"
        logpath: "/var/log/traefik/access.log"
        maxretry: 3
```

License
-------

BSD
